#!/usr/bin/python3

import json
import requests
import os

weather_key = os.environ.get("WEATHER_API_KEY")
dadata_key = os.environ.get("DADATA_API_KEY")
dadata_secret_key = os.environ.get('DADATA_SECRET_KEY')
tg_token = os.environ.get('TELEGERAM_TOKEN')

OTHER_TYPE = '''Я не могу ответить на такой тип сообщения.
Но могу ответить на:
- Текстовое сообщение с названием населенного пункта.
- Голосовое сообщение с названием населенного пункта.
- Сообщение с точкой на карте.'''

FUNC_RESPONSE = {
    'statusCode': 200,
    'body': ''
}
TELEGRAM_API_URL = f"https://api.telegram.org/bot{tg_token}"

def send_message(text, message):
    """Отправка сообщения пользователю Telegram."""
    print("in send")
    message_id = message['message_id']
    chat_id = message['chat']['id']
    reply_message = {'chat_id': chat_id,
                     'text': text,
                     'reply_to_message_id': message_id}

    requests.post(url=f'{TELEGRAM_API_URL}/sendMessage', json=reply_message)

def get_temp_address(address):
    print("in addres")
    """ Преобразование адресса в текстовом виде в координаты"""
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {key}'.format(key=dadata_key),
        'X-Secret': str(dadata_secret_key),
    }

    data = '["{adr}"]'.format(adr=address).encode()
    response = requests.post('https://cleaner.dadata.ru/api/v1/clean/address', headers=headers, data=data)

    latitude, longitude = response.json()[0]["geo_lat"], response.json()[0]["geo_lon"]
    a = requests.get(
        "http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=metric&appid={API_key}".format(
            lat=latitude,
            lon=longitude,
            API_key=weather_key))
    print(type(a.json()["main"]["temp"]))
    return a.json()["main"]["temp"]


def get_temp_coor(latitude, longitude):
    """ Преобразование координат в данные о погоде"""
    print("in coor")
    a = requests.get(
        "http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=metric&appid={API_key}".format(
            lat=latitude,
            lon=longitude,
            API_key=weather_key))
    return a.json()["main"]["temp"]


def check_message(message):
    """ Проверка сообщения на его тип """
    print("im in check")
    try:
        return get_temp_coor(message["location"]["latitude"], message["location"]["longitude"])
    except KeyError:
        try:
            return get_temp_address(message["text"])
        except KeyError:
            pass


def main(event, context):
    """Обработчик облачной функции. Реализует Webhook для Telegram Bot."""
    if tg_token is None:
        return FUNC_RESPONSE
        
    update = json.loads(event['body'])

    if 'message' not in update:
        return FUNC_RESPONSE

    message_in = update['message']
    print("get message")

    if 'text' or 'voice' or 'location' not in message_in:
        send_message(OTHER_TYPE, message_in)

    send_message(check_message(message_in), message_in)
